# Gitlab Openshift Template

## Prerequisites

* oc client installed on your machine
* An Openshift project
* update service account
```
oc adm policy add-scc-to-user anyuid system:serviceaccount:gitlab:gitlab-ce-user
```

### What does this template install

It uses the official Docker images of the following items:

* Gitlab Community Edition
* Redis
* PostGreSQL

## Ressources on Openshift

### Gitlab

```
resources:
limits:
  cpu: '1'
  memory: 2Gi
requests:
  cpu: 500m
  memory: 1Gi
```

### Redis

```
resources:
limits:
  cpu: '1'
  memory: 512Mi
requests:
  cpu: 100m
  memory: 300Mi
```

### PostGreSQL

```
resources:
limits:
  cpu: '1'
  memory: 512Mi
requests:
  cpu: 300m
  memory: 300Mi
```